const container = document.querySelector('.container');
const editClientModal = document.querySelector('.edit-client');
const editClientDelete = document.querySelector('.edit-client__delete');
const editCLientForm = document.querySelector('.edit-client__form')
const editCLientId = document.querySelector('.edit-client__id')
const editClientSaveButton = document.querySelector('.edit-client__save-btn');
const editClientAddContact = document.querySelector('.edit-client__new-contact');
const overlay = document.querySelector('.overlay');
const addClientButton = document.querySelector('.add-client__btn');
const addClientModal = document.querySelector('.add-client');
const closeIcon = document.querySelectorAll('.close-icon');
const cancelButton = document.querySelector('.add-client__cancel');
const addContact = document.querySelector('.add-client__new-contact');
const newClientForm = document.querySelector('.add-client__form');
const newClientInputName = document.getElementById('input-name');
const newClientInputSurname = document.getElementById('input-surname');
const newClientInputLastname = document.getElementById('input-lastname');
const newClientSaveButton = document.querySelector('.add-client__save-btn');
const headerInput = document.querySelector('.header__input')
let clientsArray = [];
let newClientContacts = [];
let newClientContacts1 = [];
let newClientContacts2 = [];
let clients = [];
let clientObj = {};
let contactId = 1;
let clickCount = 0;

function modalHide () {
  addClientModal.classList.remove('modal-show');
  editClientModal.classList.remove('modal-show');
  overlay.classList.remove('modal-show');
}

addClientButton.addEventListener('click', () => {
  addClientModal.classList.toggle('modal-show');
  overlay.classList.toggle('modal-show')
});

let inputName = document.getElementById('input-name');
let inputNameStar = document.querySelector('.star-name')
inputName.addEventListener('click', () => {
  inputNameStar.classList.add('hide')
});
let inputSurname = document.getElementById('input-surname');
let inputSurnameStar = document.querySelector('.star-surname')
inputSurname.addEventListener('click', () => {
  inputSurnameStar.classList.add('hide')
});

overlay.addEventListener('click', modalHide);
overlay.addEventListener('click', () => {location.reload()});
closeIcon.forEach(element => element.addEventListener('click', modalHide));
closeIcon.forEach(element => element.addEventListener('click', () => {location.reload()}));
cancelButton.addEventListener('click', modalHide);
cancelButton.addEventListener('click', () => {location.reload()});


function createContact () {
  let newContactBlock = document.createElement('div');
  newContactBlock.classList.add('new-contact-block');

  let contactType = document.createElement('div');
  let contactTypeButton = document.createElement('button');
  contactTypeButton.textContent = 'телефон';
  contactTypeButton.classList.add('active-type')
  let contactTypeList = document.createElement('ul');
  contactTypeList.classList.add('contact-type-list')
  let contactTypeTel = document.createElement('li');
  contactTypeTel.value = 'телефон';
  contactTypeTel.textContent = 'телефон';
  contactTypeTel.classList.add('contact-type');
  let contactTypeEmail = document.createElement('li');
  contactTypeEmail.value = 'email';
  contactTypeEmail.textContent = 'email';
  contactTypeEmail.classList.add('contact-type');
  let contactTypeVk = document.createElement('li');
  contactTypeVk.value = 'VK';
  contactTypeVk.textContent = 'VK';
  contactTypeVk.classList.add('contact-type');
  let contactTypeFacebook = document.createElement('li');
  contactTypeFacebook.value = 'facebook';
  contactTypeFacebook.textContent = 'facebook';
  contactTypeFacebook.classList.add('contact-type');
  let contactTypeOther = document.createElement('li');
  contactTypeOther.value = 'другое';
  contactTypeOther.textContent = 'другое';
  contactTypeOther.classList.add('contact-type');
  contactTypeList.append(contactTypeTel, contactTypeEmail, contactTypeVk, contactTypeFacebook, contactTypeOther);
  contactType.append(contactTypeButton, contactTypeList)
  
  contactTypeButton.addEventListener('click', (e) => {
    e.preventDefault();
    contactTypeList.classList.toggle('block-show')
  });

  function setType(type) {
    type.addEventListener('click', () => {
      contactTypeButton.textContent = type.textContent;
      contactTypeList.classList.remove('block-show');
    })
  }

  let typesArr = [contactTypeTel, contactTypeEmail, contactTypeVk, contactTypeFacebook, contactTypeOther];
  for (let type of typesArr) {
    setType(type)
  }

  let deleteContact = document.createElement('div');
  deleteContact.classList.add('delete-contact-block')
  deleteContact.innerHTML = `<svg class="delete-contact-icon" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M8 2C4.682 2 2 4.682 2 8C2 11.318 4.682 14 8 14C11.318 14 14 11.318 14 8C14 4.682 11.318 2 8 2ZM8 12.8C5.354 12.8 3.2 10.646 3.2 8C3.2 5.354 5.354 3.2 8 3.2C10.646 3.2 12.8 5.354 12.8 8C12.8 10.646 10.646 12.8 8 12.8ZM10.154 5L8 7.154L5.846 5L5 5.846L7.154 8L5 10.154L5.846 11L8 8.846L10.154 11L11 10.154L8.846 8L11 5.846L10.154 5Z" fill="#B0B0B0"/>
  </svg>
  `;
  
  let newContact = document.createElement('input');
  
  //newContact.id = contactId;

  return {
    contactType,
    contactTypeButton,
    newContact,
    newContactBlock,
    deleteContact,
  }
}


////////////////////////////// ОТРИСОВКА СУЩЕСТВУЮЩИХ КЛИЕНТОВ
function createPreList (data) {
  const tr = document.createElement('tr');
  const tdId = document.createElement('td');
  const tdFullName = document.createElement('td');
  const tdCreated = document.createElement('td');
  const tdModified = document.createElement('td');
  const tdContacts = document.createElement('td');
  const tdActions = document.createElement('td');
  const deleteButton = document.createElement('button');
  const editButton = document.createElement('button');

  tr.classList.add('tr');
  tdId.classList.add('td-id');
  tdFullName.classList.add('td-name');
  tdCreated.classList.add('td-created');
  tdModified.classList.add('td-modified');
  tdContacts.classList.add('td-contacts');
  tdActions.classList.add('td-actions');
  
 
  tr.classList.add('tr');
  deleteButton.classList.add('delete-btn');
  editButton.classList.add('edit-btn')

  let createdDate = new Date (data.createdAt);
  let yearCreated = createdDate.getFullYear();
  let monthCreated = ('0' + createdDate.getMonth()).slice(-2);
  let dayCreated = ('0' + createdDate.getDay()).slice(-2);
  let hourCreated = createdDate.getHours();
  let minutesCreated = createdDate.getMinutes();
  let finalCreated = `${dayCreated}.${monthCreated}.${yearCreated} <span class="time">${hourCreated}:${minutesCreated}</span>`;

  let modifiedDate = new Date (data.createdAt);
  let yearModified = modifiedDate.getFullYear();
  let monthModified = ('0' + createdDate.getMonth()).slice(-2);
  let dayModified = ('0' + createdDate.getDay()).slice(-2);
  let hourModified = createdDate.getHours();
  let minutesModified = createdDate.getMinutes();
  let finalModified = `${dayModified}.${monthModified}.${yearModified} <span class="time">${hourModified}:${minutesModified}</span>`;

  let phoneIcon = `<svg class="contact-icon" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.7">
                    <circle cx="8" cy="8" r="8" fill="#9873FF"/>
                    <path d="M11.56 9.50222C11.0133 9.50222 10.4844 9.41333 9.99111 9.25333C9.83556 9.2 9.66222 9.24 9.54222 9.36L8.84444 10.2356C7.58667 9.63556 6.40889 8.50222 5.78222 7.2L6.64889 6.46222C6.76889 6.33778 6.80444 6.16444 6.75556 6.00889C6.59111 5.51556 6.50667 4.98667 6.50667 4.44C6.50667 4.2 6.30667 4 6.06667 4H4.52889C4.28889 4 4 4.10667 4 4.44C4 8.56889 7.43556 12 11.56 12C11.8756 12 12 11.72 12 11.4756V9.94222C12 9.70222 11.8 9.50222 11.56 9.50222Z" fill="white"/>
                    </g>
                  </svg>`;
  let vkIcon = `<svg class="contact-icon" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g opacity="0.7">
                  <path d="M8 0C3.58187 0 0 3.58171 0 8C0 12.4183 3.58187 16 8 16C12.4181 16 16 12.4183 16 8C16 3.58171 12.4181 0 8 0ZM12.058 8.86523C12.4309 9.22942 12.8254 9.57217 13.1601 9.97402C13.3084 10.1518 13.4482 10.3356 13.5546 10.5423C13.7065 10.8371 13.5693 11.1604 13.3055 11.1779L11.6665 11.1776C11.2432 11.2126 10.9064 11.0419 10.6224 10.7525C10.3957 10.5219 10.1853 10.2755 9.96698 10.037C9.87777 9.93915 9.78382 9.847 9.67186 9.77449C9.44843 9.62914 9.2543 9.67366 9.1263 9.90707C8.99585 10.1446 8.96606 10.4078 8.95362 10.6721C8.93577 11.0586 8.81923 11.1596 8.43147 11.1777C7.60291 11.2165 6.81674 11.0908 6.08606 10.6731C5.44147 10.3047 4.94257 9.78463 4.50783 9.19587C3.66126 8.04812 3.01291 6.78842 2.43036 5.49254C2.29925 5.2007 2.39517 5.04454 2.71714 5.03849C3.25205 5.02817 3.78697 5.02948 4.32188 5.03799C4.53958 5.04143 4.68362 5.166 4.76726 5.37142C5.05633 6.08262 5.4107 6.75928 5.85477 7.38684C5.97311 7.55396 6.09391 7.72059 6.26594 7.83861C6.45582 7.9689 6.60051 7.92585 6.69005 7.71388C6.74734 7.57917 6.77205 7.43513 6.78449 7.29076C6.82705 6.79628 6.83212 6.30195 6.75847 5.80943C6.71263 5.50122 6.53929 5.30218 6.23206 5.24391C6.07558 5.21428 6.0985 5.15634 6.17461 5.06697C6.3067 4.91245 6.43045 4.81686 6.67777 4.81686L8.52951 4.81653C8.82136 4.87382 8.88683 5.00477 8.92645 5.29874L8.92808 7.35656C8.92464 7.47032 8.98521 7.80751 9.18948 7.88198C9.35317 7.936 9.4612 7.80473 9.55908 7.70112C10.0032 7.22987 10.3195 6.67368 10.6029 6.09801C10.7279 5.84413 10.8358 5.58142 10.9406 5.31822C11.0185 5.1236 11.1396 5.02785 11.3593 5.03112L13.1424 5.03325C13.195 5.03325 13.2483 5.03374 13.3004 5.04274C13.6009 5.09414 13.6832 5.22345 13.5903 5.5166C13.4439 5.97721 13.1596 6.36088 12.8817 6.74553C12.5838 7.15736 12.2661 7.55478 11.9711 7.96841C11.7001 8.34652 11.7215 8.53688 12.058 8.86523Z" fill="#9873FF"/>
                  </g>
                </svg>`;
  let fbIcon = `<svg class="contact-icon" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g opacity="0.7">
                  <path d="M7.99999 0C3.6 0 0 3.60643 0 8.04819C0 12.0643 2.928 15.3976 6.75199 16V10.3775H4.71999V8.04819H6.75199V6.27309C6.75199 4.25703 7.94399 3.14859 9.77599 3.14859C10.648 3.14859 11.56 3.30121 11.56 3.30121V5.28514H10.552C9.55999 5.28514 9.24799 5.90362 9.24799 6.53815V8.04819H11.472L11.112 10.3775H9.24799V16C11.1331 15.7011 12.8497 14.7354 14.0879 13.2772C15.3261 11.819 16.0043 9.96437 16 8.04819C16 3.60643 12.4 0 7.99999 0Z" fill="#9873FF"/>
                  </g>
                </svg>`;
  let emailIcon = `<svg class="contact-icon" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.7" fill-rule="evenodd" clip-rule="evenodd" d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM4 5.75C4 5.3375 4.36 5 4.8 5H11.2C11.64 5 12 5.3375 12 5.75V10.25C12 10.6625 11.64 11 11.2 11H4.8C4.36 11 4 10.6625 4 10.25V5.75ZM8.424 8.1275L11.04 6.59375C11.14 6.53375 11.2 6.4325 11.2 6.32375C11.2 6.0725 10.908 5.9225 10.68 6.05375L8 7.625L5.32 6.05375C5.092 5.9225 4.8 6.0725 4.8 6.32375C4.8 6.4325 4.86 6.53375 4.96 6.59375L7.576 8.1275C7.836 8.28125 8.164 8.28125 8.424 8.1275Z" fill="#9873FF"/>
                  </svg>`;
  let otherIcon = `<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.7" fill-rule="evenodd" clip-rule="evenodd" d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16ZM3 8C3 5.24 5.24 3 8 3C10.76 3 13 5.24 13 8C13 10.76 10.76 13 8 13C5.24 13 3 10.76 3 8ZM9.5 6C9.5 5.17 8.83 4.5 8 4.5C7.17 4.5 6.5 5.17 6.5 6C6.5 6.83 7.17 7.5 8 7.5C8.83 7.5 9.5 6.83 9.5 6ZM5 9.99C5.645 10.96 6.75 11.6 8 11.6C9.25 11.6 10.355 10.96 11 9.99C10.985 8.995 8.995 8.45 8 8.45C7 8.45 5.015 8.995 5 9.99Z" fill="#9873FF"/>
                  </svg>`;
                 
  

  tdId.textContent = data.id.substr(0, 6);
  tdFullName.textContent = `${data.name} ${data.surname} ${data.lastName}`;
  tdCreated.innerHTML = finalCreated;
  tdModified.innerHTML = finalModified;
  for (let i = 0; i < data.contacts.length; i++) {
    if (data.contacts[i].type == 'телефон') {tdContacts.innerHTML += `<div class=icon-block>${phoneIcon}<div class="tooltip"><span class="tooltip-text">${data.contacts[i].value}</span></div></div>`}
    if (data.contacts[i].type == 'VK') {tdContacts.innerHTML += `<div class=icon-block>${vkIcon}<div class="tooltip"><span class="tooltip-text">${data.contacts[i].value}</span></div></div>`}
    if (data.contacts[i].type == 'facebook') {tdContacts.innerHTML += `<div class=icon-block>${fbIcon}<div class="tooltip"><span class="tooltip-text">${data.contacts[i].value}</span></div></div>`}
    if (data.contacts[i].type == 'другое') {tdContacts.innerHTML += `<div class=icon-block>${otherIcon}<div class="tooltip"><span class="tooltip-text">${data.contacts[i].value}</span></div></div>`}
    if (data.contacts[i].type == 'email') {tdContacts.innerHTML += `<div class=icon-block>${emailIcon}<div class="tooltip"><span class="tooltip-text">${data.contacts[i].value}</span></div></div>`}
    /*tdContacts.textContent += `${data.contacts[i].type}: ${data.contacts[i].value} `*/
  }
  deleteButton.innerHTML = `
  <svg class="edit-delete-icons" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.7">
    <path d="M8 2C4.682 2 2 4.682 2 8C2 11.318 4.682 14 8 14C11.318 14 14 11.318 14 8C14 4.682 11.318 2 8 2ZM8 12.8C5.354 12.8 3.2 10.646 3.2 8C3.2 5.354 5.354 3.2 8 3.2C10.646 3.2 12.8 5.354 12.8 8C12.8 10.646 10.646 12.8 8 12.8ZM10.154 5L8 7.154L5.846 5L5 5.846L7.154 8L5 10.154L5.846 11L8 8.846L10.154 11L11 10.154L8.846 8L11 5.846L10.154 5Z" fill="#F06A4D"/>
    </g>
  </svg>

  Удалить`;
  editButton.innerHTML = `
  <svg class="edit-delete-icons" width="16" height="16" viewbox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g opacity="0.7">
    <path d="M2 11.5002V14.0002H4.5L11.8733 6.62687L9.37333 4.12687L2 11.5002ZM13.8067 4.69354C14.0667 4.43354 14.0667 4.01354 13.8067 3.75354L12.2467 2.19354C11.9867 1.93354 11.5667 1.93354 11.3067 2.19354L10.0867 3.41354L12.5867 5.91354L13.8067 4.69354Z" fill="#9873FF"/>
    </g>
  </svg>
  Изменить`;

  //////////////////// РЕДАКТИРОВНИЕ И УДАЛЕНИЕ
  deleteButton.addEventListener('click', () => {
    alert(`delete ${data.id}`);
    deleteItem(data.id)
    tr.remove();
  })


  editButton.addEventListener('click', () => {
    function clearEditForm () {
      contactsToRemove = document.querySelectorAll('.new-contact-block');
      contactsToRemove.forEach(el => el.remove())
    }
    clearEditForm()
    editCLientId.textContent = data.id
    editClientModal.classList.add('modal-show');
    overlay.classList.add('modal-show');
    const editNameInput = document.getElementById('input-name-edit');
    const editSurnameInput = document.getElementById('input-surname-edit');
    const editLastameInput = document.getElementById('input-lastname-edit');
    editNameInput.value = data.name;
    editSurnameInput.value = data.surname
    editLastameInput.value = data.lastName

    editClientDelete.addEventListener('click', () => {
      alert(`delete ${data.id}`);
      deleteItem(data.id)
      tr.remove();
      editClientModal.classList.remove('modal-show');
      overlay.classList.remove('modal-show');
    })

    for (let contact of data.contacts) {
      const contactCreator = createContact();
      
      contactCreator.newContact.classList.add('edit-client__input-contact');
      contactCreator.newContactBlock.append(contactCreator.contactType, contactCreator.newContact, contactCreator.deleteContact);      
      contactCreator.contactTypeButton.textContent = contact.type;
      contactCreator.newContact.value = contact.value;
      contactCreator.deleteContact.addEventListener('click', () => {
        contactCreator.newContactBlock.remove();
      })
      editCLientForm.append(contactCreator.newContactBlock);      
    }
    
    let clicks = 0;
    editClientAddContact.addEventListener('click', () => {
      
      clicks++;
      const contactCreator = createContact();
      
      contactCreator.newContact.classList.add('edit-client__input-contact');
      contactCreator.newContactBlock.append(contactCreator.contactType, contactCreator.newContact, contactCreator.deleteContact); 
      contactCreator.deleteContact.addEventListener('click', () => {
        contactCreator.newContactBlock.remove();
      })
      editCLientForm.append(contactCreator.newContactBlock);
      
      if (data.contacts.length + clicks === 5) {
        editClientAddContact.classList.add('hide');        
      }
      else {editClientAddContact.classList.remove('hide');}
    });

    
    if (data.contacts.length === 5) {
      editClientAddContact.classList.add('hide');
      //clicks = 0;
    }
    else {editClientAddContact.classList.remove('hide');}
    console.log(data.contacts.length)


    editClientSaveButton.addEventListener('click', async () => {

      const contactType = document.querySelectorAll('.active-type');
      const contactValue = document.querySelectorAll('.edit-client__input-contact');
      let contacts = [];
      let clientObj = {};

      for (i=0; i<contactType.length; i++) {
        contacts.push({
          type:contactType[i].textContent,
          value:contactValue[i].value
        })
      }

      clientObj.name = editNameInput.value;
      clientObj.surname = editSurnameInput.value;
      clientObj.lastName = editLastameInput.value;
      clientObj.contacts = contacts;

      const response = await fetch(`http://localhost:3000/api/clients/${data.id}`, {
          method: 'PATCH',
          headers: {'Content-type': 'application/json'},
          body: JSON.stringify({
            name:editNameInput.value,
            surname:editSurnameInput.value,
            lastName:editLastameInput.value,
            contacts:contacts
          })
      });

        //editedClient = await response.json();

      location.reload();
    });
  })
  //////////////////// РЕДАКТИРОВНИЕ И УДАЛЕНИЕ

  tdActions.append(editButton, deleteButton)
  tr.append(tdId, tdFullName, tdCreated, tdModified, tdContacts, tdActions);
  clientsArray.push(tr)
  return tr
}
 ////////////////////////////// ОТРИСОВКА СУЩЕСТВУЮЩИХ КЛИЕНТОВ


////////////////////////////// серверная часть тест
async function dataGetter () {
  const response = await fetch ('http://localhost:3000/api/clients', {
    method: 'GET',
  });
  const data = await response.json();
  //console.log(data)
  return data
}

async function deleteItem (id) {
  const response = await fetch(`http://localhost:3000/api/clients/${id}`, {
    method: 'DELETE',
  });
}

////////////////////////////// серверная часть

////////////////////////////// таблица
const tableContainer = document.querySelector('.table-container');

const tableHeader = document.createElement('h1');
tableHeader.classList.add('table-header');
tableHeader.textContent = 'КЛИЕНТЫ';

const table = document.createElement('table');
table.classList.add('table');

///////////// прелоадер
const preloader = document.createElement('div');
preloader.classList.add('preloader')
const preloaderRow = document.createElement('div');
preloaderRow.classList.add('preloader__row')
const preloaderItemOne = document.createElement('div');
preloaderItemOne.classList.add('preloader__item')
const preloaderItemTwo = document.createElement('div');
preloaderItemTwo.classList.add('preloader__item')
preloaderRow.append(preloaderItemOne, preloaderItemTwo);
preloader.append(preloaderRow)
///////////// прелоадер

const tableRow = document.createElement('tr');

const tableHeadId = document.createElement('th');
tableHeadId.innerHTML = `ID 
<svg class="arrow-icon" width="12" height="12" viewbox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.7">
<path d="M10 6L9.295 5.295L6.5 8.085L6.5 2H5.5L5.5 8.085L2.71 5.29L2 6L6 10L10 6Z" fill="#9873FF"/>
</g>
</svg>`;

tableHeadId.classList.add('th-id');

const tableHeadFullName = document.createElement('th');
tableHeadFullName.innerHTML = `Фамилия Имя Отчество 
<svg class="arrow-icon" width="12" height="12" viewbox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.7">
<path d="M10 6L9.295 5.295L6.5 8.085L6.5 2H5.5L5.5 8.085L2.71 5.29L2 6L6 10L10 6Z" fill="#9873FF"/>
</g>
</svg>
<svg class="letters-icon" width="16" height="8" viewbox="0 0 16 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M5.37109 8L4.6582 6.01758H1.92871L1.23047 8H0L2.6709 0.832031H3.94043L6.61133 8H5.37109ZM4.35059 5.01172L3.68164 3.06836C3.63281 2.93815 3.56445 2.73307 3.47656 2.45312C3.39193 2.17318 3.33333 1.9681 3.30078 1.83789C3.21289 2.23828 3.08431 2.67611 2.91504 3.15137L2.27051 5.01172H4.35059ZM6.96289 5.80762V4.83105H9.47266V5.80762H6.96289ZM13.0322 5.13867L11.2646 8H9.93164L11.9434 4.87012C11.0319 4.55436 10.5762 3.8903 10.5762 2.87793C10.5762 2.22363 10.8024 1.72396 11.2549 1.37891C11.7074 1.03385 12.373 0.861328 13.252 0.861328H15.3955V8H14.2236V5.13867H13.0322ZM14.2236 1.83789H13.2959C12.8044 1.83789 12.4268 1.92578 12.1631 2.10156C11.9027 2.27409 11.7725 2.55729 11.7725 2.95117C11.7725 3.33529 11.8994 3.63477 12.1533 3.84961C12.4072 4.06445 12.8011 4.17188 13.335 4.17188H14.2236V1.83789Z" fill="#9873FF"/>
</svg>
`;

tableHeadFullName.classList.add('th-name')

const tableHeadCreated = document.createElement('th');
tableHeadCreated.innerHTML = `Дата создания 
<svg class="arrow-icon" width="12" height="12" viewbox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.7">
<path d="M10 6L9.295 5.295L6.5 8.085L6.5 2H5.5L5.5 8.085L2.71 5.29L2 6L6 10L10 6Z" fill="#9873FF"/>
</g>
</svg>`;
tableHeadCreated.classList.add('th-created')

const tableHeadModified = document.createElement('th');
tableHeadModified.innerHTML = `Последние изменения 
<svg class="arrow-icon" width="12" height="12" viewbox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.7">
<path d="M10 6L9.295 5.295L6.5 8.085L6.5 2H5.5L5.5 8.085L2.71 5.29L2 6L6 10L10 6Z" fill="#9873FF"/>
</g>
</svg>`;
tableHeadModified.classList.add('th-modified')

const tableHeadContacts = document.createElement('th');
tableHeadContacts.textContent = 'Контакты';
tableHeadContacts.classList.add('th-contacts')

const tableHeadActions = document.createElement('th');
tableHeadActions.textContent = 'Действия';
tableHeadActions.classList.add('th-actions')

tableRow.append(tableHeadId, tableHeadFullName, tableHeadCreated, tableHeadModified, tableHeadContacts, tableHeadActions);
table.append(preloader, tableRow);
tableContainer.append(tableHeader, table);
////////////////////////////// таблица  


///////////////////////////// ДОБОАВЛЕНИЕ КОНТАКТОВ
addContact.addEventListener('click', () => {
  let test = createContact()
  test.newContact.classList.add('add-client__input-contact');
  clickCount++;
  if (clickCount == 5) {
    test.newContactBlock.append(test.contactType, test.newContact, test.deleteContact)
    newClientForm.append(test.newContactBlock);
    //addContact.remove()
    addContact.classList.add('hide');
  }
  else {
    test.newContactBlock.append(test.contactType, test.newContact, test.deleteContact)
    newClientForm.append(test.newContactBlock);
  }
  test.deleteContact.addEventListener('click', () => {
    test.newContactBlock.remove();
  })
  contactId++;
  if (clickCount > 0) {newClientContacts2.push(test.newContactBlock)}
});
///////////////////////////// ДОБОАВЛЕНИЕ КОНТАКТОВ


/////////////////// ДОБАВЛЕНИЕ КЛИЕНТА
newClientSaveButton.addEventListener('click', async () => {
  newClientContacts = document.querySelectorAll('.add-client__input-contact');
  newClientContactsType = document.querySelectorAll('.active-type');
  const newContact = document.querySelector('.add-client__input-contact');
  const newContacts = document.querySelectorAll('.add-client__input-contact');
  let contactsTypeValue = [];  
  
  const addClientBottom = document.querySelector('.add-client__bottom');
  const alertName = document.createElement('span');
  const alertLength = document.createElement('span');
  const alertContact = document.createElement('span');
  alertName.textContent = 'не стоит писать тут цифры';
  alertName.classList.add('alert-name', 'alert');
  alertLength.classList.add('alert-length', 'alert');
  alertContact.classList.add('alert-contact', 'alert');
  alertContact.textContent = 'либо удаляем, либо заполняем полностью'
  alertLength.textContent = 'мало букв';
  alertName.style.color = 'red';
  alertName.style.fontSize = '12px';
  alertLength.style.color = 'red';
  alertLength.style.fontSize = '12px';
  alertContact.style.color = 'red';
  alertContact.style.fontSize = '12px';

  let alerts = document.querySelectorAll('.alert');
  alerts.forEach(el => el.remove());
  
  if(/\d/.test(newClientInputName.value) || /\d/.test(newClientInputSurname.value) || /\d/.test(newClientInputLastname.value)) {
    newClientInputName.style.borderColor = 'red';
    newClientInputSurname.style.borderColor = 'red';
    newClientInputLastname.style.borderColor = 'red';
    addClientBottom.prepend(alertName)
    return
  }
  if(newClientInputName.value.trim().length < 2 || newClientInputSurname.value.trim().length < 2 || newClientInputLastname.value.trim().length < 2) {
    newClientInputName.style.borderColor = 'red'
    newClientInputSurname.style.borderColor = 'red';
    newClientInputLastname.style.borderColor = 'red';
    addClientBottom.prepend(alertLength)
    return
  }
  if (newContact.value.length < 5) {
    newContact.style.borderColor = 'red';
    newContacts.forEach(el=> el.style.borderColor = 'red')
    addClientBottom.prepend(alertContact)
    return
  }

  for (let i=0; i<newClientContactsType.length; i++) {
    contactsTypeValue.push({
      type:newClientContactsType[i].textContent,
      value:newClientContacts[i].value,
    })
  } 

  clientObj.name = newClientInputName.value.trim();
  clientObj.surname = newClientInputSurname.value.trim();
  clientObj.lastname = newClientInputLastname.value.trim();
  clientObj.contacts = contactsTypeValue;
  
  const response = await fetch('http://localhost:3000/api/clients', {
    method: 'POST',
    headers: {'Content-type': 'application/json'},
    body: JSON.stringify({
      name:newClientInputName.value.trim(),
      surname:newClientInputSurname.value.trim(),
      lastName:newClientInputLastname.value.trim(),
      contacts:contactsTypeValue
    })
  });
  const newClient  = await response.json();
  

  const tr = document.createElement('tr');
  const tdId = document.createElement('td');
  const tdFullName = document.createElement('td');
  const tdCreated = document.createElement('td');
  const tdModified = document.createElement('td');
  const tdContacts = document.createElement('td');
  const tdActions = document.createElement('td');
  const deleteButton = document.createElement('button');
  const editButton = document.createElement('button');
 
  tr.classList.add('tr');
  tdId.classList.add('td-id');
  tdFullName.classList.add('td-name');
  tdCreated.classList.add('td-created');
  tdModified.classList.add('td-modified');
  tdContacts.classList.add('td-contacts');
  tdActions.classList.add('td-actions');
  deleteButton.classList.add('delete-btn');
  editButton.classList.add('edit-btn')
  tdId.classList.add('tdId');
  tdFullName.classList.add('tdName');
  
  tdId.textContent = newClient.id.substr(0, 8);

  
  tdFullName.textContent = `${clientObj.name} ${clientObj.surname} ${clientObj.lastname}`  
  
  let dateCreate = newClient.createdAt;
  tdCreated.textContent = dateCreate;  
  
  let dateModified = newClient.updatedAt;
  tdModified.textContent = dateModified;  
  
  for (let i = 0; i < contactsTypeValue.length; i++) {
    tdContacts.textContent += `${contactsTypeValue[i].type}: ${contactsTypeValue[i].value} `
  }  
  deleteButton.textContent = `delete`;
  editButton.textContent = `edit`
  
  tdActions.append(deleteButton, editButton)
  tr.append(tdId, tdFullName, tdCreated, tdModified, tdContacts, tdActions);
  table.append(tr);

  //clientsArray.push(tr);
  
  newClientContacts2.forEach(elem => {
    elem.remove()
  });
  newClientContacts1.splice(0, newClientContacts1.length);
  clickCount = 0;
  
    if (clickCount < 5) {
      addContact.classList.remove('hide');
    }
    
    newClientInputName.value = ``;
    newClientInputSurname.value = ``;
    newClientInputLastname.value = ``;

    
  location.reload()
  /*addClientModal.classList.add('modal-show');
  overlayclassList.add('modal-show');*/
});
/////////////////// ДОБАВЛЕНИЕ КЛИЕНТА

newClientInputName.addEventListener('input', () => {
  newClientInputName.style.borderColor = 'grey'
});
newClientInputSurname.addEventListener('input', () => {
  newClientInputSurname.style.borderColor = 'grey'
});
newClientInputLastname.addEventListener('input', () => {
  newClientInputLastname.style.borderColor = 'grey'
});


const newContacts = document.querySelectorAll('.add-client__input-contact');
addClientModal.addEventListener('click', (e) => {
  target = e.target;
  if(target.closest('.add-client__input-contact')) {
    newContacts.forEach(el => el.value = 'grey')
  }
})

async function createCRM() {
  let clients = await dataGetter();
  for (let client of clients) {
    table.append(createPreList(client))
  }
  console.log(clients)
}

createCRM()

function clearList() {
  clientsArray.forEach(el => el.remove())
}

let switchSort = 0;

tableHeadId.addEventListener('click', async ()=> {
  switchSort++;
  //alert('sort by id')
  let clients = await dataGetter();
  let clientsSortedById = [];
  if (switchSort == 1){
    clientsSortedById = clients.sort(function(a,b) {
      if(a.id > b.id) {
        return -1
      }
      if (a.id < b.id) {
        return 1
      }
      return 0
    })
    clearList();
    for (let client of clientsSortedById) {
      table.append(createPreList(client))
    }
  }
  if(switchSort == 2) {
    clientsSortedById = clients.sort(function(a,b) {
      if(a.id > b.id) {
        return 1
      }
      if (a.id < b.id) {
        return -1
      }
      return 0
    })
    switchSort = 0;
  }
  clearList();
  for (let client of clientsSortedById) {
    table.append(createPreList(client))
  }
});

tableHeadFullName.addEventListener('click', async ()=> {
  switchSort++;
  //alert('sort by id')
  let clients = await dataGetter();
  let clientsSortedById = [];
  if (switchSort == 1){
    clientsSortedById = clients.sort(function(a,b) {
      if(a.surname > b.surname) {
        return -1
      }
      if (a.surname < b.surname) {
        return 1
      }
      return 0
    })
    clearList();
    for (let client of clientsSortedById) {
      table.append(createPreList(client))
    }
  }
  if(switchSort == 2) {
    clientsSortedById = clients.sort(function(a,b) {
      if(a.surname > b.surname) {
        return 1
      }
      if (a.surname < b.surname) {
        return -1
      }
      return 0
    })
    switchSort = 0;
  }
  clearList();
  for (let client of clientsSortedById) {
    table.append(createPreList(client))
  }
});

tableHeadCreated.addEventListener('click', async ()=> {
  switchSort++;
  //alert('sort by id')
  let clients = await dataGetter();
  let clientsSortedById = [];
  if (switchSort == 1){
    clientsSortedById = clients.sort(function(a,b) {
      if(a.createdAt > b.createdAt) {
        return -1
      }
      if (a.createdAt < b.createdAt) {
        return 1
      }
      return 0
    })
    clearList();
    for (let client of clientsSortedById) {
      table.append(createPreList(client))
    }
  }
  if(switchSort == 2) {
    clientsSortedById = clients.sort(function(a,b) {
      if(a.createdAt > b.createdAt) {
        return 1
      }
      if (a.createdAt < b.createdAt) {
        return -1
      }
      return 0
    })
    switchSort = 0;
  }
  clearList();
  for (let client of clientsSortedById) {
    table.append(createPreList(client))
  }
});

tableHeadModified.addEventListener('click', async ()=> {
  switchSort++;
  //alert('sort by id')
  let clients = await dataGetter();
  let clientsSortedById = [];
  if (switchSort == 1){
    clientsSortedById = clients.sort(function(a,b) {
      if(a.updatedAt > b.updatedAt) {
        return -1
      }
      if (a.updatedAt < b.updatedAt) {
        return 1
      }
      return 0
    })
    clearList();
    for (let client of clientsSortedById) {
      table.append(createPreList(client))
    }
  }
  if(switchSort == 2) {
    clientsSortedById = clients.sort(function(a,b) {
      if(a.updatedAt > b.updatedAt) {
        return 1
      }
      if (a.updatedAt < b.updatedAt) {
        return -1
      }
      return 0
    })
    switchSort = 0;
  }
  clearList();
  for (let client of clientsSortedById) {
    table.append(createPreList(client))
  }
});

async function filter() {
  //alert(headerInput.value)
  let clients = await dataGetter();
  let clientsFiltered = [];

  clientsFiltered = clients.filter(el => 
     headerInput.value == `${el.name}` || 
     headerInput.value == `${el.surname}` ||
     headerInput.value == `${el.lastName}` ||
     headerInput.value == `${el.name} ${el.surname}` ||
     headerInput.value == `${el.name} ${el.lastName}` ||
     headerInput.value == `${el.surname} ${el.name}` ||
     headerInput.value == `${el.surname} ${el.lastName}` ||
     headerInput.value == `${el.lastName} ${el.surname}` ||
     headerInput.value == `${el.lastName} ${el.name}` ||
     headerInput.value == `${el.name} ${el.surname} ${el.lastName}` ||
     headerInput.value == `${el.surname} ${el.name} ${el.lastName}` ||
     headerInput.value == el.id ||
     headerInput.value == el.id.substr(0, 6) ||
     headerInput.value == el.createdAt.includes(headerInput.value) ||
     headerInput.value == el.updatedAt.includes(headerInput.value)

  );
  clearList();
  for (let client of clientsFiltered) {
    table.append(createPreList(client))
  }
  console.log(clientsFiltered)
}
headerInput.addEventListener('change', () => {
  setTimeout(filter, 1000)
})

window.onload = function () {
  document.body.classList.add('loaded_hiding');
  window.setTimeout(function () {
    document.body.classList.add('loaded');
    document.body.classList.remove('loaded_hiding');
  }, 500);
}